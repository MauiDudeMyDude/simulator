#include "MyString.h"
#include "characterHelper.h"

#define EOS '\0'
#define EOL '\n'
#define SPACE ' '

MyString::MyString(){
  length = 0;
  size = 1;
  characters = new char[size];
  characters[0] = EOS;
}


//PRE: constructor
//POST: size is the size of the string and characters contains the string sent
MyString::MyString(const char * word){
  length = getStringLength(word);
  size = length + 1;
  characters = new char[size];
  copyString(word, characters);
  characters[length] = EOS;
}

//PRE: copy constructor
//POST: all the values from pString have been properly copied over
MyString::MyString(const MyString & pString){
  size = pString.size;
  length = pString.length;
  characters = new char[size];
  copyString(pString.characters, characters);
  characters[length] = EOS;
}

//PRE: deconstructor
//POST: deletes everything
MyString::~MyString(){
  delete[] characters;
  characters = NULL;
  size = 0;
  length = 0;
}

//PRE: ch is a defined character
//POST: characters is what it was with ch added just before the EOS character
void MyString::addCharacter(char ch){
  if (length == size - 1){
    size *= 2;
    char * temp = new char[size];
    copyString(characters, temp);
    temp[length] = ch;
    length++;
    temp[length] = EOS;
    delete[] characters;
    characters = temp;
  }else{
    characters[length] = ch;
    length++;
    characters[length] = EOS;
  }
}

//PRE: pString is defined "\0" and stream is a defined input stream
//POST: stream contains pString
istream & operator >> (istream & stream, MyString & pString){
  char ch = stream.get();
  while((ch == SPACE) || (ch == EOL)){
    ch = stream.get();
  }
  while(ch != SPACE){
    pString.addCharacter(ch);
    ch = stream.get();
  }
  return stream;
}

//PRE: pString is defined "...\0" and stream is a defined output stream
//POST: RV = ostream contains pString
ostream & operator << (ostream & stream, const MyString & pString){
  if (pString.length == 0) {
    stream << " is empty.";
  }
  else {
    stream << pString.characters;
  }
  return stream;
} 

//PRE: pString is defined "...\0"
//POST: RV = this concatenated with pString
MyString MyString::operator + (const MyString & pString) const{
  MyString tempString(characters);
  for (uint index = 0; index < pString.length; index++){
    tempString.addCharacter(pString.characters[index]);
  }
  return (tempString);
}

//PRE: word is defined "..."
//POST: RV = this concatenated with word
MyString MyString::operator + (const char * word) const{
  MyString tempString(characters);
  uint index = 0;
  while(word[index] != EOS){
    tempString.addCharacter(word[index]);
    index++;
  }
  return (tempString);
}

//PRE: ch is defined 'x' x being some character
//POST: RV = this concatenated with ch
MyString MyString::operator + (const char ch) const{
  MyString tempString(characters);
  tempString.addCharacter(ch);
  return (tempString);
}


//PRE: index is an index of characters < size
//POST: RV = characters[index]
char & MyString::operator [] (const uint index){
  return characters[index];
}

//PRE: pString is defined "...\0"
//POST: RV = MyString that is not pString but defined "...\0"
MyString & MyString::operator = (const MyString & pString){
  delete[] characters;
  characters = new char[pString.length + 1];
  characters[0] = EOS;
  length = 0;
  size = pString.length + 1;
  copyString (pString.characters, characters);
  length = pString.length;
  characters[length] = EOS;
  return (*this);
}

//PRE: word is defined "...\0"
//POST: RV = MyString that is defined "...\0"
MyString & MyString::operator = (const char * word){
  delete[] characters;
  uint strLength = getStringLength(word);
  characters = new char[strLength + 1];
  characters[0] = EOS;
  length = 0;
  size = strLength + 1;
  copyString(word, characters);
  length = strLength;
  characters[length] = EOS;
  return (*this);
}

//PRE: pString is defined "...\0"
//POST: RV = if this and pString contain the same String
bool MyString::operator == (const MyString & pString) const{
  bool answer = true;
  uint index = 0;
  while((characters[index] != EOS) && (answer)){
    if (pString.characters[index] != characters[index]){
      answer = false;
    }
    index++;
  }
  if(answer){
    answer = (characters[index] == pString.characters[index]);
  }
  return answer;
}

//PRE: word is defined "...\0"
//POST: RV = if this and word contain the same String
bool MyString::operator == (const char * word) const{
  MyString temp(word);
  return (*this == temp);
}

//PRE: pString is defined "...\0"
//POST: RV = this < pString, < meaning alphabeticaly
bool MyString::operator < (const MyString & pString) const{
  uint answer = 0;
  uint index = 0;
  while (characters[index] != EOS && answer == 0){
    if (characters[index] < pString.characters[index]){
      answer = 1;
    }else if(characters[index] > pString.characters[index]){
      answer = 2;
    }
    index++;
  }
  if(answer == 0){
    if(pString.characters[index] == EOS){
      answer = 2;
    }else{
      answer = 1;
    }
  }
  if(answer == 2){
    answer = 0;
  }
  return ((bool)answer);
}

//PRE: constructor has been called
//POST: RV = size
uint MyString::getSize() const{
  return size;
}

//PRE: constructor has been called
//POST: RV = length
uint MyString::getLength() const{
  return length;
}

//PRE: Initialized
//POST: RV = a char[] == characters
char * MyString::cstr() const{
  char * answer = new char[length + 1];
  copyString(characters, answer);
  answer[length] = EOS;
  return answer;
}

//PRE: Initialized
//POST: returns a MyString object containing this values but characters is in reverse order
MyString MyString::getReverse() const{
  MyString temp;
  for (uint index = length - 1; index > -1; index--){
    temp.addCharacter(characters[index]);
  }
  cout << "temp: " << temp << endl;
  return (temp);
}
