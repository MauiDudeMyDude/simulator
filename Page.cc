#include "Page.h"
#include "constants.h"

//Default Constructor
Page::Page(){
  pid = 0;
  id = 0;
  stackspace = false;
  dirty = false;
  empty = true;
}

//Constructor
Page::Page(uint pPID, uint pID, bool pStackSpace){
  pid = pPID;
  id = pID;
  stackspace = pStackSpace;
  dirty = false;
  empty = false;
}

//Getters
uint Page::getPID()const{
  return pid;
}
uint Page::getID()const{
  return id;
}
bool Page::getStackSpace()const{
  return stackspace;
}
bool Page::getDirty()const{
  return dirty;
}
bool Page::getEmpty()const{
  return empty;
}

//Setters
void Page::setPID(const uint pPID){
  pid = pPID;
}
void Page::setID(const uint pID){
  id = pID;
}
void Page::setStackSpace(const bool pStackSpace){
  stackspace = pStackSpace;
}
void Page::setDirty(const bool pDirty){
  dirty = pDirty;
}
void Page::setEmpty(const bool pEmpty){
  empty = pEmpty;
}

//PRE: other is defined
//POST: RV = true iff other is the same as this
bool Page::operator == (const Page & other){
  bool answer = (pid == other.pid);
  if(answer){
    answer = (id == other.id);
    if(answer){
      answer = (stackspace == other.stackspace);
      if(answer){
	answer = (empty == other.empty);
      }
    }
  }
  return answer;
}

//PRE: other is defined
//POST: RV = stream << other
ostream & operator << (ostream & stream, Page & other){
  stream << "PID: " << other.pid << " ID: " << other.id << " Stack: ";
  if(other.stackspace){
    stream << "true ";
  }else{
    stream << "false ";
  }
  stream << "Dirty: ";
  if(other.dirty){
    stream << "true ";
  }else{
    stream << "false ";
  }
  stream << " Empty: ";
  if(other.empty){
    stream << "true";
  }else{
    stream << "false";
  }
  return stream;
}
