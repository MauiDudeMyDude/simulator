#ifndef INCLUDED_helper
#define INCLUDED_helper
#include "constants.h"
#include "CPU.h"
#include "MyString.h"

//PRE: input is required
//POST: RV = a mystring containing the entire line inputed
MyString readLine();

//PRE: input contains a command and arguements if necesary and is 
//     and command is empty
//POST: RV = only the command in input
MyString pullCommand(MyString input);

//PRE: command is MAXCOMMANDSIZE big
//POST: RV = True iff command is in COMMANDS and false otherwise
bool realCommand(MyString command);

//PRE: computer is the CPU object that contains memory and handles all commands
//     passed to it by shell
//POST: reads in commands until a valid command is entered. if the command is
//     exits the program end otherwise the command is sent to computer
void shell(CPU & computer);

//PRE: id is the pid of the object that needs input
//POST: RV = what is inputed into the shell
uint shellInput(MyString id);


//PRE: str is no greater than MAXLENGTHOFMAXMEMORY and it is not negative
//POST: RV = the number in str as a uint
uint convertToUint(MyString str);

//PRE: number has no more digits than MAXINPUT
//POST: RV = a mystring containing number
MyString convertToString(uint number);

//PRE: input contains "'command' 'attribute(1)'...'attribute(n)'" and
//     attributeNo is the attribute to return and attributeNo <= n
//POST: RV = 'attribute(attributeNo)'
uint getAttribute(MyString input, uint attributeNo);

//PRE: input contains a command and some number of attributes
//POST: RV = 1 + number of attributes
uint numArguements(MyString input);

//PRE: config is CONFIGARGUEMENTS big
//POST: config[0] = memory from .lc_config
//      config[1] = stack from .lc_config
//      config[2] = mem-management from .lc_config
void readConfig(uint * config);

#endif
