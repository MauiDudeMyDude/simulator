#ifndef INCLUDED_CPU
#define INCLUDED_CPU

#include "constants.h"
#include "Memory.h"
#include "MyString.h"
#include "LinkedList.h"
#include "PCB.h"
#include "Block.h"
#include "Page.h"
#include "FileSystem.h"

class CPU{
private:
  //PC
  uint pc;

  //config data
  uint stack;
  uint timeslice;
  uint pagesize;
  uint swapspace;
  uint paging;

  //next pid
  uint pid;
  
  //free memory
  LinkedList<Block> freemem;

  //running queue
  LinkedList<PCB> runningQueue;

  //Frame Table
  Page * frameTable;

  //Swap Space
  fstream * swap_space;

  //Page Queue
  LinkedList<Page> pageQueue;
  
  //Registers
  uint * registers;

  //Memory
  Memory memory;

  //File System
  FileSystem files;

  //Private Methods
  //PRE: pageQueue has at least one page
  //POST: RV = the index in frameTable of pageQueue[0]
  uint next();

  //PRE: page may or may not be empty
  //POST: RV = true iff page is empty and false otherwise
  bool emptyPage(uint & page);

  //PRE: pageTable is created
  //POST: RV = index of the next place to put a page
  uint findNextPage();

  //PRE: page may or may not be in the frameTable
  //POST: RV = true iff page is in the frameTable and false otherwise
  bool inFrameTable(Page page);

  //PRE: page is not in frameTable or pageQueue
  //POST: page is in the frameTable and pageQueue
  void pageFault(Page page);

  //PRE: page is a page in frametable
  //POST: RV = index of page
  uint getMemWord(Page page);

  //PRE: pg is a page to add to the frame Table page is the index
  //     to place it at and temp is the PCB that holds pg
  //POST: pg is in the frameTable
  void addPage(Page pg, uint page, PCB temp);

  //PRE: pc is the program counters current position
//     sw is true iff this was called in save word
  //POST: RV = the location in memory pc is at and if
  //      its not in memory it will load it in
  uint getLocationInMemory(uint pc, bool sw);
  
  //PRE: word2 might be negative
  //POST: RV = word1 + word2
  uint addOffset(uint word1, uint word2);
  
  //PRE: input contains "load 'prog'"
  //POST: RV = a mystring containing 'prog'
  MyString getProgName(MyString input);

  //PRE: pid is the job id to remove from ram
  //POST: the job with pid of pid has been removed from ram
  void ramDealocate(uint pid);
  
  //PRE: prog is the name of the program. 
  //POST: loads in the first page of the program and stack into ram and also creates
  //      a PID object for that program onto the back of the queue
  //      RV = a string with a msg saying if it was successful or not
  MyString load(MyString & prog);

  //PRE: start if given is where to start and similarly with end
  //POST: Display the contents of memory, four words on each line, starting at
  //      the word at address start and ending with the word at address end. If
  //      start and/or end are not word addresses, i.e., multiples of 4, then
  //      your program should give the user a suitable error message and then
  //      show the simulator prompt. Your display should include the address of
  //      the location and the contents, both in hexadecimal (8 digits) followed
  //      by the decimal value of the contents in parentheses.
  //      If only one value is given after the mem instruction, then all words
  //      of memory starting at that address should be displayed.
  //      If no value is given after the mem instruction, then all words of
  //      memory should be displayed.
  MyString mem(uint start, uint end);

  //PRE: only called if the shell is given "cpu"
  //POST: Display the contents of the PC on one line. Following that, display
  //      the contents of all the registers, four registers on each line. The
  //      contents of the PC and the registers should be displayed in
  //      hexadecimal (8 digits) and in decimal in parentheses. All the contents
  //      should be suitably annotated, i.e., PC: 0x00000144, etc. When
  //      displaying the registers, use the names for the registers, i.e., $a0,
  //      $s0, etc. You may also find it useful to display in parentheses the
  //      decimal values of the contents of the PC and registers.
  MyString cpu();

  //PRE: step is how many steps of slice has been executed slice is the number
  //     of time slices that have been run n is the number
  //     of time slices to run line contains either a 0 or it contains a line
  //     + a helper word to deterimine how to handle the program
  //POST: run n time slices and return a MyString wich contains the outputs and
  //      line will contain instructions to get input if needed
  MyString runHelper(uint & step, uint & slice, uint n, MyString & line);
  
  //PRE: step is how many steps of the current time slice has been run line
  //     contains either a 0 or it contains a line + a helper word to determine
  //     how to handle the program
  //POST: Run 1 timeslices, or until the instruction halt is executed,
  //      whichever comes earlier, of the machine language program, starting at
  //      the current value of PC, and display the LC-2200 prompt.
  MyString runStep(uint & step, MyString & line);

  //PRE: only called if the shell is given "run"
  //POST: Run the machine language program, starting at the current value of PC,
  //      until, if ever, the instruction halt is executed.
  //MyString run(MyString & line);

  //PRE: Initialized
  //POST: Display the process information for all the processes in the running
  //      queue, starting with the process at the front of the queue. For each
  //      process, the simulator should print out the PID for the process, the
  //      name of the program for the process, the PC for the process, the starting
  //      address of the memory allocaated to the process, the number of words of
  //      memory allocated to the process, the starting address of stack space
  //      allocated to the process, the ending address of the stack space allocated
  //      to the process, and the value of the stack pointer for the process. 
  MyString jobs();

  //PRE: pid may be the pid of a job
  //POST: RV = index of job with pid and size if job doesn't exist
  uint getJob(uint pid);

  //PRE: Initialized
  //POST: Remove from the running queue the process, if any, with the specified PID,
  //      and free all memory, including stack space, allocated to that process. 
  MyString kill(uint id);

  //PRE: frame table is initialized
  //POST: RV = a Mystring containing the frame table
  MyString printFrameTable();

  //PRE: pid may be the pid of a job
  //POST: RV = a mystring containing the page table of job pid
  MyString printPageTable(uint pid);
  
public:
  //PRE: all parameters contain the values that were preset in the .lc_config file
  //POST: This object has been initialized
  CPU(uint wordsOfMemory, uint wordsOfStack, uint pTimeslice, uint pPageSize,
      uint pSwapSpace, uint pPaging, uint fileSysSize);

  //Deconstructor
  ~CPU();

  //PRE: step and slice contain info only for step input contains a proper command
  //     and any atributes that go with it
  //POST: RV = true iff execute was able to properly execute the command without
  //      any problems and false if there was an error in execution
  void execute(uint & step, uint & slice, MyString input, MyString & msg, MyString & line);

};

#endif
