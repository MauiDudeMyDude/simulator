#ifndef INCLUDED_INODE
#define INCLUDED_INODE

#include "MyString.h"
#include "constants.h"

class Inode{
 private:
  uint data[INODEWORDS][BYTESINWORD];

  //PRE: bytetwo is zero and num is the number that
  //     should be split into two bytes
  //POST: byte two contains the left byte and num
  //      contains the right byte
  void split(uint & bytetwo, uint & num);
  
 public:
  //PRE: Constructor
  //POST: Makes this a Inode of type free
  Inode();

  //PRE: type is the type of Inode this is
  //POST: Makes this a Inode of type type
  Inode(MyString type);

  //PRE: word is the word the byte is in and byte is
  //     which byte you want
  //POST: RV = the contents of byte byte in word word
  //      of data
  uint getByte(uint word, uint byte);

  //PRE: word is the word the bytes are in and firstByte is
  //     the first byte you want of data
  //POST: RV = the contents of bytes firstByte and firstByte + 1
  //      as if they were 1 in word word of data
  uint getBytes(uint word, uint firstByte);

  //PRE: word is the word and byte is the byte of
  //     data you want to hold val 
  //POST: byte byte of word word of data = val
  void setByte(uint word, uint byte, uint val);

  //PRE: word is the word and firstByte is the first byte of
  //     data you want to hold val
  //POST: bytes firstByte and firstByte+1 of word word are set
  //      to val
  void setBytes(uint word, uint firstByte, uint val);
  
};

#endif
