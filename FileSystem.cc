#include "FileSystem.h"
#include "constants.h"
#include "MyString.h"
#include "Inode.h"
#include <fstream>
using namespace std;

//PRE: path is the path to the directory you want
//POST: RV = the index of the inode at the directory defined by path
uint FileSystem::getIndexAt(MyString path){
  uint index = currentInode;
  uint spot = 0;
  if(path[spot] == '/'){
    index = 1;
    spot++;
  }
  MyString token;
  bool error = false;
  while(path[spot] != EOS){
    if(path[spot] == '/'){
      spot++;
    }
    //token = getToken(spot, path);
    //Needs to check if token is a child of inodes[index]
    //if it is index will be set to that inodes index
    //else it will error and quit the for loop
  }
  return index;
}

FileSystem::FileSystem(){
  numInodes = 0;
}

//PRE: pNumInodes is greater than 2
//POST: pwd = "/" inodes = new Inode[pNumInodes]
//      numInodes = pNumInodes inodes[0] will be
//      a super inode and inodes[1] will be the root
//      directory inode
FileSystem::FileSystem(uint pNumInodes){
  PWD = (char*)"/";
  numInodes = pNumInodes;
  inodes = new Inode[pNumInodes];
  format();
  currentInode = 1;
}

//PRE: defined
//POST: RV = numInodes
uint FileSystem::getNumInodes(){
  return numInodes;
}

//PRE: inodes is defined
//POST: inodes[0] will be the super inode, inodes[1] will
//      be the root directory with no children, and all
//      other inodes will be free and in order
//      2, 3, ..., numInodes - 1
void FileSystem::format(){
  //Super Block
  inodes[0].setBytes(0, 0, numInodes - 2);
  inodes[0].setBytes(0, 2, numInodes);
  inodes[0].setBytes(1, 0, 2);
  inodes[0].setBytes(1, 2, numInodes - 1);
  //Root
  inodes[1].setByte(0, 0, 0);
  inodes[1].setByte(0, 1, (uint)'/');
  inodes[1].setByte(0, 2, 0);
  inodes[1].setBytes(5, 2, 0);
  inodes[1].setBytes(6, 0, 0);
  inodes[1].setBytes(6, 2, 0);
  inodes[1].setBytes(7, 1, 0);
  inodes[1].setBytes(7, 2, 0);
  //Free Inodes
  for(uint index = 2; index < numInodes; index++){
    inodes[index].setByte(0, 0, 3);
    if(index > 2){
      inodes[index].setBytes(7, 0, index - 1);
    }else{
      inodes[index].setBytes(7, 0, 0);
    }
    if(index < numInodes-1){
      inodes[index].setBytes(7, 2, index + 1);
    }else{
      inodes[index].setBytes(7, 2, 0);
    }
  }
}

//PRE: defined
//POST: RV = pwd
MyString FileSystem::pwd(){
  return PWD;
}

//PRE: path is a path to the directory wanted
//POST: lists the contents of the directory defined by path
MyString FileSystem::ls(MyString path){
  uint tempCurrent = currentInode;
  if(!(path == NOPATH)){
    getIndexAt(path);
  }
}

//PRE: path is a path to the directory wanted
//POST: changes to the directory specified by path. if path
//      is an invalid directory an error message will be returned
//      otherwise a success message will be returned
MyString FileSystem::cd(MyString path){
  MyString msg = (char*)"Not Working";
  return msg;
}

//PRE: path is a path to the directory wanted
//POST: if path is a valid path to create the directory it will be
//      created with the first free inode. if there are no more free
//      inodes or the path is invalid a proper error message will be
//      returned otherwise a success message will be returned
MyString FileSystem::mkdir(MyString path){
  MyString msg = (char*)"Not Working";
  return msg;
}

//PRE: path is a path to the directory wanted
//POST: if the directory exists and has no children it will be deleted
//      and a success message will be returned otherwise a proper error
//      error message will be returned
MyString FileSystem::rmdir(MyString path){
  MyString msg = (char*)"Not Working";
  return msg;
}

//PRE: start is less than or equal to end
//POST: RV = all inodes from start to end inclusive will be formated like
//      "Inode No. 'Inode Index'
//       'word 1' 'word 2' 'word 3' 'word 4'
//       'word 5' 'word 6' 'word 7' 'word 8'"
//      in to a MyString
MyString FileSystem::displayInode(uint start, uint end){
  MyString msg;
  char buffer[MAXMESSAGE];
  for(uint index = start; index <= end; index++){
    sprintf(buffer, "Inode No. %d\n", index);
    msg = msg + buffer;
    sprintf(buffer, "0x%04x%04x 0x%04x%04x 0x%04x%04x 0x%04x%04x\n", inodes[index].getBytes(0, 0),
	    inodes[index].getBytes(0, 2), inodes[index].getBytes(1, 0),
	    inodes[index].getBytes(1, 2), inodes[index].getBytes(2, 0),
	    inodes[index].getBytes(2, 2), inodes[index].getBytes(3, 0),
	    inodes[index].getBytes(3, 2));
    msg = msg + buffer;
    sprintf(buffer, "0x%04x%04x 0x%04x%04x 0x%04x%04x 0x%04x%04x\n\n", inodes[index].getBytes(4, 0),
	    inodes[index].getBytes(4, 2), inodes[index].getBytes(5, 0),
	    inodes[index].getBytes(5, 2), inodes[index].getBytes(6, 0),
	    inodes[index].getBytes(6, 2), inodes[index].getBytes(7, 0),
	    inodes[index].getBytes(7, 2));
    msg = msg + buffer;
  }
  return msg;
}
