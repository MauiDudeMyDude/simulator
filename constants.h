#ifndef INCLUDED_constants
#define INCLUDED_constants
#include "MyString.h"

//Constants
#define EOS '\0'
#define SPACE ' '
#define NL '\n'
#define ASCIIZERO 48
#define MAXCOMMANDSIZE 13
#define MAXINPUT 20
#define NUMCOMMANDS 17
#define MAXPROGLENGTH 15
#define LOADOFFSET 5
#define WORDSIZE 32
#define NUMREGISTERS 16
#define MAXREGISTERLENGTH 6
#define BYTESINWORD 4
#define MAXATTRIBUTE 10
#define MAXLENGTHOFMAXMEMORY 4
#define OPCODESHIFT 7
#define REGXSHIFT 6
#define REGYSHIFT 5
#define MAXLINE 5
#define MAXMESSAGE 80
#define MAXFILENAME 20
#define CONFIGARGUEMENTS 7
#define INODEWORDS 8
#define TWOBYTES 256
#define uint unsigned int

//Enums
enum opcodes {ADD, NAND, ADDI, LW, SW, BEQ, JALR, HALT, DUMMYEIGHT, IN, OUT, DUMMYB, LA, BGT};
enum commands {LOADINDEX, MEMINDEX, CPUINDEX, STEPINDEX, RUNINDEX, FREEMEMINDEX,
	       JOBSINDEX, KILLINDEX, PAGESINDEX, FORMATINDEX, PWDINDEX, LSINDEX,
	       CDINDEX, MKDIRINDEX, RMDIRINDEX, DISPLAYINODEINDEX, EXITINDEX};
enum registers {ZERO, AT, V0, A0, A1, A2, T0, T1, T2, S0, S1, S2, K0, SP, FP, RA};

//Character Arrays
static const char COMMANDS[NUMCOMMANDS][MAXCOMMANDSIZE] = {"load", "mem", "cpu", "step", "run",
							   "freemem", "jobs", "kill", "pages",
							   "format", "pwd", "ls", "cd", "mkdir",
							   "rmdir", "displayInode", "exit"};
static const char REGISTERNAMES[NUMREGISTERS][MAXREGISTERLENGTH] = {"$zero", "$at", "$v0", "$a0",
								    "$a1", "$a2", "$t0", "$t1",
								    "$t2", "$s0", "$s1", "$s2",
								    "$k0", "$sp", "$fp", "$ra"};

static const MyString NOPATH((char*)"no path");

//running checks
static const MyString DONE((char*)"done");
static const MyString INPUT((char*)"in");
static const MyString HALTED((char*)"halt");

//Config Parameters
static const MyString MEMORY((char*)"memory");
static const MyString STACK((char*)"stack");
static const MyString MEMMANAGEMENT((char*)"mem-management");
static const MyString TIMESLICE((char*)"timeslice");
static const MyString PAGESIZE((char*)"pagesize");
static const MyString SWAPSPACE((char*)"swapspace");
static const MyString PAGING((char*)"paging");
static const MyString FILESYSSIZE((char*)"FileSysSize");

#endif
