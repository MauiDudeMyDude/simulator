Created by Maui Kelley 2/10/2018

Instructions for how to use LC2200

in the terminal
	$cd mlkelley-Simulator
	$make
	$./LC2200 <words of memory wanted>
	for example of 5 words of memory:
		$./LC2200 5

TestCases.txt contains a description and name
of all test cases

Files Needed:
	simulator.cc
	helper.cc
	helper.h
	CPU.cc
	CPU.h
	constants.h
	Memory.cc
	Memory.h
	Makefile
	TestCases.txt
	TestCase1.lce
	TestCase2.lc
	TestCase2.lce
	README.md
