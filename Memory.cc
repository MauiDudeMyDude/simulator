#include "constants.h"
#include "Memory.h"
#include <iostream>

using namespace std;

//Default Constructor
Memory::Memory(){
  memory = new uint[0];
  numWords = 0;
}

//Constructor
Memory::Memory(uint pNumWords){
  memory = new uint[pNumWords];
  numWords = pNumWords;
}

//Destructor
Memory::~Memory(){
  delete[] memory;
  numWords = 0;
}

//PRE: word is a full word of memory and index is where to insert it into mem
//POST: word is stored at memory[index]
void Memory::addWord(uint word, uint index){
  memory[index] = word;
}

//PRE: initialized
//POST: RV = numWords
uint Memory::getNumWords(){
  return numWords;
}

//PRE: pNumWords is how many words of memory you want this to hold
//POST: numWords = pNumWords and memory is a empty uint[numWords]
void Memory::setNumWords(uint pNumWords){
  numWords = pNumWords;
  delete[] memory;
  memory = new uint[numWords];
}

//PRE: address is an valid and defined address of a word in memory
//POST: RV = the word at address address
uint Memory::getWord(uint address){
  return memory[address / BYTESINWORD];
}

//PRE: word is no more then 8 bytes and address is a valid address of a word
//     in memory
//POST: stores word at the address address
void Memory::setWord(uint word, uint address){
  memory[address / BYTESINWORD] = word;
}
