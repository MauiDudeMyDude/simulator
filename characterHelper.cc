#include "characterHelper.h"
#include <iostream>

using namespace std;

#define EOS '\0'

//PRE: string1 is defined "...\0" and string2 has enough space to hold string1
//POST: string2 contains all characters from string1 up to \0
void copyString(const char * string1, char * string2){
  int index = 0;
  while (string1[index] != EOS){
    string2[index] = string1[index]; 
    index++;
  }
}

//PRE: word is defined "...\0"
//POST: RV = number of characters before '\0'
int getStringLength(const char * word){
  int length = 0;
  while (word[length] != EOS){
    length++;
  }
  return length;
}
