#ifndef INCLUDED_characterHelper
#define INCLUDED_characterHelper

//PRE: string1 is defined "...\0" and string2 has enough space to hold string1
//POST: string2 contains all characters from string1 up to \0
void copyString(const char * string1, char * string2);

//PRE: word is defined "...\0"
//POST: RV = number of characters before '\0'
int getStringLength(const char * word);

#endif
