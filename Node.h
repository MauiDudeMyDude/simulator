#ifndef INCLUDED_Node
#define INCLUDED_Node

#include <iostream>

using namespace std;

template <class T>
class Node{
 private:
  T data;
  Node<T> * next;
  Node<T> * prev;
 public:
  //CONSTRUCTORS
  //PRE: Default constructor
  //POST: Initializes variables
  Node(){
    next = NULL;
    prev = NULL;
  };

  //PRE: pData is of type T. T being what type assigned to this template
  //POST: Initializes variables
  Node(T pData){
    data = pData;
    next = NULL;
    prev = NULL;
  };

  //PRE: Copy Constructor
  //POST: Initialises variables = pNode
  Node(const Node<T> & pNode){
    data = pNode.data;
    next = pNode.next;
    prev = pNode.prev;
  };

  //DECONSTRUCTOR
  //PRE: Initialized
  //POST: No more
  ~Node(){
    if (next != NULL){
      delete next;
    }
  };
  
  //ACCESSORS
  //PRE: Initialized
  //POST: RV = data
  T getData()const {
    return data;
  };

  //PRE: Initialized
  //POST: RV = a reference to data
  T & getRefData(){
    return data;
  };

  //PRE: Initialized
  //POST: RV = next
  Node<T> * getNext()const{
    return next;
  };

  //PRE: Initialized
  //POST: RV = prev
  Node<T> * getPrev()const {
    return prev;
  };

  //SETTERS
  //PRE: pData is defined
  //POST: data = pData
  void setData(const T pData){
    data = pData;
  };

  //PRE: pNext is defined
  //POST: next = pNext
  void setNext(Node<T> * pNext){
    next = pNext;
  };

  //PRE: pPrev is defined
  //POST: prev = pPrev
  void setPrev(Node<T> * pPrev){
    prev = pPrev;
  };
  
};

#endif
