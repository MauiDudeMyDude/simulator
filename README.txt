Created by Maui Kelley 4/15/2018

Instructions for how to use LC2200

in the terminal
	$cd mlkelley-TimeSlice
	$make clean
	$make
	$./LC2200


Program has format, pwd, and displayInode working except if you don't give 
displayInode values it will segfault.  Nothing else works

Files Needed:
	simulator.cc
	helper.cc
	helper.h
	CPU.cc
	CPU.h
	constants.h
	Memory.cc
	Memory.h
	MyString.cc
	MyString.h
	characterHelper.cc
	characterHelper.h
	Block.cc
	Block.h
	LinkedList.h
	Node.h
	PCB.cc
	PCB.h
	Page.cc
	Page.h
	Makefile
	FileSystem.cc
	FileSystem.h
	Inode.cc
	Inode.h
	README.txt
	.lc_config
	FileSystemTestCases/
		TestCases.txt

