#ifndef INCLUDED_Page
#define INCLUDED_Page
#include "constants.h"
#include <iostream>
using namespace std;

class Page{
 private:
  uint pid;
  uint id;
  bool stackspace;
  bool dirty;
  bool empty;
 public:
  //Default Constructor
  Page();
  
  //Constructor
  Page(uint pPid, uint pId, bool pStackSpace);

  //Getters
  uint getPID()const;
  uint getID()const;
  bool getStackSpace()const;
  bool getDirty()const;
  bool getEmpty()const;

  //Setters
  void setPID(const uint pPID);
  void setID(const uint pID);
  void setStackSpace(const bool pStackSpace);
  void setDirty(const bool pDirty);
  void setEmpty(const bool pEmpty);

  //PRE: other is defined
  //POST: RV = true iff other is the same as this
  bool operator == (const Page & other);

  //PRE: other is defined
  //POST: RV = stream << other
  friend ostream & operator << (ostream & stream, Page & other);
};

#endif
