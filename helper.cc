#include "helper.h"
#include "constants.h"
#include "CPU.h"
#include "MyString.h"
#include <iostream>
#include <fstream>

using namespace std;

//PRE: input is required
//POST: RV = a mystring containing the entire line inputed
MyString readLine(){
  char character = cin.get();
  MyString input;
  uint index = 0;
  do{
    if(character != NL){
      input = input + character;
    }
    character = cin.get();
    index++;
  }while(character != NL);
  return input;
}

//PRE: input contains a command and arguements if necesary and is 
//     and command is empty
//POST: RV = only the command in input
MyString pullCommand(MyString input){
  MyString command;
  int index = 0;
  while(input[index] != SPACE && input[index] != EOS){
    command = command + input[index];
    index++;
  }
  return command;
}

//PRE: command is MAXCOMMANDSIZE big
//POST: RV = True iff command is in COMMANDS and false otherwise
bool realCommand(MyString command){
  bool answer = false;
  for(int index = 0; index < NUMCOMMANDS; index++){
    if(command == COMMANDS[index]){
      answer = true;
    }
  }
  return answer;
}

//PRE: computer is the CPU object that contains memory and handles all commands
//     passed to it by shell
//POST: reads in commands until a valid command is entered. if the command is
//     exit the program will end otherwise the command is sent to computer
void shell(CPU & computer){
  bool commandFound = false;
  bool stop = false;
  MyString input;
  MyString command;
  MyString msg;
  MyString line;
  uint step = 0;
  uint slice = 0;
  //ASSERT: all necesary variables are initialized
  while(!stop){
    //ASSERT: Exit has not been called yet
    line = (char*)"0";
    while((!commandFound)){
      //ASSERT: A valid command has not been called
      cout << ">";
      input = readLine();
      //ASSERT: input contains the entire command line
      command = pullCommand(input);
      //ASSERT: command contains the command entered
      commandFound = realCommand(command);
      //ASSERT: commandFound = true iff commmand is a valid command
    }
    if(!(command == COMMANDS[EXITINDEX])){
      //ASSERT: The valid command is not exit
      while(!(line == DONE)){
	//ASSERT: The proccess isn't done and input is the entire call and msg contains
	//        garbage and line contains the line to continue the operation with
	computer.execute(step, slice, input, msg, line);
	//ASSERT: execute has returned
	if(pullCommand(line) == INPUT){
	  //ASSERT: line is the string "in <line> <id>" indicating that the CPU needs input
	  uint in = shellInput(convertToString(getAttribute(line, 2)));
	  //ASSERT: in contains the value inputed
	  MyString lineStr = convertToString(getAttribute(line, 1));
	  line = lineStr + (char*)(" ");
	  MyString inStr = convertToString(in);
	  line = line + inStr;
	  //ASSERT: line is the string "<line> <in>"
	}
	//ASSERT: msg contains what needs to be printed 
	if(msg.getLength() > 0){
	  cout << msg << endl;
	}
	//ASSERT: msg has been printed
      }
      //ASSERT: the command has been processed
      step = 0;
      slice = 0;
    }else{
      //ASSERT: The command exit was called
      stop = true;
    }
    commandFound = false;    
  }
  //ASSERT: exit has been called
}

//PRE: id is the pid of the object that needs input
//POST: RV = what is inputed into the shell
uint shellInput(MyString id){
  cout << id + (char*)">";
  uint input;
  cin >> input;
  return input;
}


//PRE: str is no greater than MAXLENGTHOFMAXMEMORY and it is not negative
//POST: RV = the number in str as a uint
uint convertToUint(MyString str){
  uint rv = 0;
  uint size = str.getLength();
  for(uint index = 0; index < size; index++){
    rv = (rv * 10) + (str[index] - ASCIIZERO);
  }
  return rv;
}


//PRE: number has no more digits than MAXINPUT
//POST: RV = a mystring containing number
MyString convertToString(uint number){
  char str[MAXINPUT];
  sprintf(str, "%d", number);
  MyString rv(str);
  return rv;
}

//PRE: input contains "'command' 'attribute(1)'...'attribute(n)'" and
//     attributeNo is the attribute to return and attributeNo <= n
//POST: RV = 'attribute(attributeNo)'
uint getAttribute(MyString input, uint attributeNo){
  uint cursor = 0;
  uint counter = 0;
  while(counter < attributeNo){
    if(input[cursor] == SPACE){
      counter++;
    }
    cursor++;
  }
  //ASSERT: cursor = first index of attributeNo
  uint start = cursor;
  MyString attribute;
  do{
    attribute = attribute + input[cursor];
    cursor++;
  }while(input[cursor] != EOS && input[cursor] != SPACE);
  //ASSERT: attribute contains the attribute(attributeNo)
  uint attr = convertToUint(attribute);
  //attr contains the attribute(attributeNo)
  return attr;
}


//PRE: input contains a command and some number of attributes
//POST: RV = 1 + number of attributes
uint numArguements(MyString input){
  uint counter = 1;
  uint cursor = 0;
  while(input[cursor] != EOS){
    if(input[cursor] == SPACE){
      counter++;
    }
    cursor++;
  }
  return counter;
}

//PRE: config is CONFIGARGUEMENTS big
//POST: config[0] = memory from .lc_config
//      config[1] = stack from .lc_config
//      config[2] = timeslice from .lc_config
//      config[3] = pagesize from .lc_config
//      config[4] = swapspace from .lc_config
//      config[5] = paging from .lc_config
void readConfig(uint * config){
  MyString word;
  uint val;
  fstream file(".lc_config");
  for(uint counter = 0; counter < CONFIGARGUEMENTS; counter++){
    word = (char*)"";
    file >> word;
    file >> val;
    //ASSERT: word contains the keyword and val contains it's value
    uint index;
    if(word == MEMORY){
      index = 0;
    }else if(word == STACK){
      index = 1;
    }else if(word == TIMESLICE){
      index = 2;
    }else if(word == PAGESIZE){
      index = 3;
    }else if(word == SWAPSPACE){
      index = 4;
    }else if(word == PAGING){
      index = 5;
    }else if(word == FILESYSSIZE){
      index = 6;
    }
    //ASSERT: index = the index of config that needs the current val
    config[index] = val;
  }
  //ASSERT: config[0] = memory from .lc_config
  //        config[1] = stack from .lc_config
  //        config[2] = timeslice from .lc_config
  //        config[3] = pagesize from .lc_config
  //        config[4] = swapspace from .lc_config
  //        config[5] = paging from .lc_config
  //        config[6] = FileSysSize from .lc_config
}
