LC2200:	simulator.o helper.o CPU.o Memory.o MyString.o characterHelper.o PCB.o Block.o Page.o FileSystem.o Inode.o
	g++ simulator.o helper.o CPU.o Memory.o MyString.o characterHelper.o PCB.o Block.o Page.o FileSystem.o Inode.o -o LC2200

simulator.o:	simulator.cc helper.h CPU.h MyString.h
	g++ -c simulator.cc

helper.o:	helper.cc helper.h constants.h CPU.h MyString.h
	g++ -c helper.cc

CPU.o:	CPU.cc CPU.h Memory.h helper.h constants.h MyString.h LinkedList.h PCB.h Block.h Page.h FileSystem.h
	g++ -c CPU.cc

Memory.o:	Memory.cc Memory.h constants.h MyString.h
	g++ -c Memory.cc

MyString.o:	MyString.cc MyString.h characterHelper.h
	g++ -c MyString.cc

characterHelper.o:	characterHelper.cc characterHelper.h
	g++ -c characterHelper.cc

PCB.o:	PCB.cc PCB.h constants.h MyString.h Page.h
	g++ -c PCB.cc

Block.o:	Block.cc Block.h constants.h
	g++ -c Block.cc

Page.o:	Page.cc Page.h constants.h
	g++ -c Page.cc

FileSystem.o:	FileSystem.cc FileSystem.h constants.h MyString.h Inode.h

Inode.o:	Inode.cc Inode.h constants.h  MyString.h

clean:
	rm LC2200
	rm simulator.o
	rm helper.o
	rm CPU.o
	rm Memory.o
	rm MyString.o
	rm characterHelper.o
	rm PCB.o
	rm Block.o
	rm Page.o
	rm FileSystem.o
	rm Inode.o

run:
	./LC2200
