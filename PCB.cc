#include "PCB.h"
#include "MyString.h"
#include "constants.h"
#include <fstream>
#include "Page.h"

//Private
//PRE: pRegisters is a define array of size NUMREGISTERS
//POST: registers is a deep copy of pRegisters
void PCB::copyRegisters(const uint * pRegisters){
  for(uint index = 0; index < NUMREGISTERS; index++){
    registers[index] = pRegisters[index];
  }
}

//PRE: pFrameTable is defined and pSize is its size
//POST: pageTable is a deep copy of pFrameTable
void PCB::copyFrameTable(const Page * pFrameTable, const uint pSize){
  for(uint index = 0; index < pSize; index++){
    pageTable[index] = pFrameTable[index];
  }
}

//PRE: pagesize is the size of each page and stackspace is how much stack space the prog has
//POST: the frame table is completed
void PCB::makeFrameTable(uint pagesize, uint stackspace){
  uint numWords;
  (*program) >> numWords;
  //get is to get \n character
  program->get();
  uint stackpages = stackspace/pagesize;
  uint pages = numWords/pagesize;
  if(numWords % pagesize != 0){
    pages++;
  }
  pageTable = new Page[pages + stackpages];
  for(int index = 0; index < pages; index++){
    pageTable[index].setPID(pid);    
    pageTable[index].setID(index);   
    pageTable[index].setEmpty(false);
  }
  size = pages + stackpages;
  for(int index = 0; index < stackpages; index++){
    pageTable[size - 1 - index].setPID(pid);
    pageTable[size - 1 - index].setID(index);
    pageTable[size - 1 - index].setStackSpace(true);   
    pageTable[size - 1 - index].setEmpty(false);
  }
}

//Public
//Default constructor
PCB::PCB(){
  pc = 0;
  pid = 0;
  name = (char*)("");
  pageTable = NULL;
  program = NULL;
  registers = new uint[NUMREGISTERS];
  size = 0;
}

//Constructor
PCB::PCB(uint id, uint * pRegisters, MyString pName, uint pagesize, uint stackspace){
  pc = 0;
  pid = id;
  registers = new uint[NUMREGISTERS];
  copyRegisters(pRegisters);
  name = pName;
  MyString fileName = name + (char*)".lce";
  char * file_name = fileName.cstr();
  program = new fstream(file_name);
  delete[] file_name;
  makeFrameTable(pagesize, stackspace);
  registers[SP] = (size - 1) * BYTESINWORD;
}

//Copy Constructor
PCB::PCB(const PCB & block){
  pc = block.pc;
  name = block.name;
  pid = block.pid;
  size = block.size;
  registers = new uint[NUMREGISTERS];
  copyRegisters(block.registers);
  pageTable = new Page[size];
  copyFrameTable(block.pageTable, size);
  program = block.program;
}

//Destructor
PCB::~PCB(){
  delete[] registers;
  delete[] pageTable;
}
  
//PRE: pc defined
//POST: RV = pc
uint PCB::getPC()const{
  return pc;
}
  
//PRE: pid defined
//POST: RV = pid
uint PCB::getID()const{
  return pid;
}

//PRE: size defined
//POST: RV = size
uint PCB::getSize()const{
  return size;
}
  
//PRE: registers defined
//POST: RV = registers
uint * PCB::getRegisters(){
  return registers;
}

//PRE: name defined
//POST: RV = name
MyString PCB::getName()const{
  return name;
}

//PRE: program defined
//POST: RV = program
fstream * PCB::getProg()const{
  return program;
}

//PRE: pageTable defined
//POST: RV = pageTable
Page * PCB::getPageTable()const{
  return pageTable;
}

//Setters
//PRE: defined
//POST: pc = pPC
void PCB::setPC(const uint pPC){
  pc = pPC;
}

//PRE: defined
//POST: pid = id
void PCB::setID(const uint id){
  pid = id;
}
  
//PRE: defined
//POST: registers = pregisters
void PCB::setRegisters(const uint pRegisters[NUMREGISTERS]){
  copyRegisters(pRegisters);
}

//PRE: defined
//POST: name = pName
void PCB::setName(MyString pName){
  name = pName;
}

//PRE: pageTable initialized
//POST: RV = MyString containing the page table
MyString PCB::printPageTable(){
  MyString msg;
  char buffer[MAXMESSAGE];
  for(int index = 0; index < size; index++){
    msg = msg + (char*)"=====================\n";
    sprintf(buffer, "PID %d ID: %d Stack: ", pageTable[index].getPID(), pageTable[index].getID());
    msg = msg + buffer;
    if(pageTable[index].getStackSpace()){
      msg = msg + (char*)"true ";
    }else{
      msg = msg + (char*)"false ";
    }
    msg = msg + (char*)"Dirty: ";
    if(pageTable[index].getDirty()){
      msg = msg + (char*)"true ";
    }else{
      msg = msg + (char*)"false ";
    }
    msg = msg + (char*)"Empty: ";
    if(pageTable[index].getEmpty()){
      msg = msg + (char*)"true";
    }else{
      msg = msg + (char*)"false";
    }
    msg = msg + (char*)"\n";
  }
  return msg;
}

//PRE: block is defined
//POST: RV = PCB that is not block but a deep copy
PCB & PCB::operator = (const PCB & block){
  pc = block.pc;
  name = block.name;
  pid = block.pid;
  size = block.size;
  delete[] registers;
  registers = new uint[NUMREGISTERS];
  copyRegisters(block.registers);
  delete[] pageTable;
  pageTable = new Page[size];
  copyFrameTable(block.pageTable, block.size);
  program = block.program;
  return (*this);
}

//PRE: other is defined
//POST: RV = true iff other.pid == pid
bool PCB::operator ==(const PCB & other){
  return (pid == other.pid);
}
  
//PRE: chunkblock is defined and stream is a defined output stream
//POST: stream contains chunkblock
ostream & operator << (ostream & stream, const PCB & block){
  stream << "[" << block.name << ", " << block.pid << "]";
  return stream;
}
