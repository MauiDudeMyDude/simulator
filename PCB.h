#ifndef INCLUDED_PCB
#define INLUCDED_PCB
#include "constants.h"
#include "MyString.h"
#include <fstream>
#include "Page.h"

class PCB{
 private:
  //PCs
  uint pc;

  //Name
  MyString name;
  
  //ID
  uint pid;

  //Size
  uint size;

  //page table
  Page * pageTable;

  //program
  fstream * program;
  
  //Registers
  uint * registers;

  //PRE: pRegisters is a define array of size NUMREGISTERS
  //POST: registers is a deep copy of pRegisters
  void copyRegisters(const uint pRegisters[NUMREGISTERS]);

  //PRE: pFrameTable is defined
  //POST: pageTable is a deep copy of pFrameTable
  void copyFrameTable(const Page * pFrameTable, const uint pSize);

  //PRE: pagesize is the size of each page and stackspace is how much stack space the prog has
  //POST: the frame table is completed
  void makeFrameTable(uint pagesize, uint stackspace);

 public:
  //Default constructor
  PCB();

  //Constructor
  PCB(uint id, uint * pRegisters, MyString pName, uint pagesize, uint stackspace);

  //Copy Constructor
  PCB(const PCB & block);

  //Destructor
  ~PCB();

  //Getters
  //PRE: defined
  //POST: RV = realativePC
  uint getRealativePC()const;
  
  //PRE: pc defined
  //POST: RV = pc
  uint getPC()const;
  
  //PRE: pid defined
  //POST: RV = pid
  uint getID()const;

  //PRE: size defined
  //POST: RV = size
  uint getSize()const;
  
  //PRE: registers defined
  //POST: RV = registers
  uint * getRegisters();

  //PRE: name defined
  //POST: RV = name
  MyString getName()const;

  //PRE: program defined
  //POST: RV = program
  fstream * getProg()const;

  //PRE: pageTable defined
  //POST: RV = pageTable
  Page * getPageTable()const;

  //Setters
  //PRE: defined
  //POST: pc = pPC
  void setPC(const uint pPC);
  
  //PRE: defined
  //POST: pid = id
  void setID(const uint id);
  
  //PRE: defined
  //POST: registers = pRegisters
  void setRegisters(const uint pRegisters[NUMREGISTERS]);

  //PRE: defined
  //POST: name = pName
  void setName(MyString pName);

  //PRE: pageTable initialized
  //POST: RV = MyString containing the page table
  MyString printPageTable();

  //PRE: block is defined
  //POST: RV = PCB that is not block but a deep copy
  PCB & operator = (const PCB & block);

  //PRE: other is defined
  //POST: RV = true iff other.pid == pid
  bool operator ==(const PCB & other);

  //PRE: chunkblock is defined and stream is a defined output stream
  //POST: stream contains chunkblock
  friend ostream & operator << (ostream & stream, const PCB & block);
};

#endif
