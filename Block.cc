#include "Block.h"
#include "constants.h"

//Default Constructor
Block::Block(){
  start = 0;
  end = 0;
  size = 0;
}

//Constructor
Block::Block(uint pStart, uint pEnd, uint pSize){
  start = pStart;
  end = pEnd;
  size = pSize;
}

//getters
//PRE: start defined
//POST: RV = start
uint Block::getStart()const{
  return start;
}

//PRE: end defined
//POST: RV = end
uint Block::getEnd()const{
  return end;
}

//PRE: size defined
//POST: RV = size
uint Block::getSize()const{
  return size;
}

//setters
//PRE: defined
//POST: start = pStart
void Block::setStart(const uint pStart){
  start = pStart;
}

//PRE: defined
//POST: end = pEnd
void Block::setEnd(const uint pEnd){
  end = pEnd;
}

//PRE: defined
//POST: size = pSize
void Block::setSize(const uint pSize){
  size = pSize;
}

//PRE: chunk is no larger than size
//POST: size = size - chunk and start = the new starting address
void Block::reduce(const uint chunk){
  size -= chunk;
  start += (chunk * BYTESINWORD);
}

//PRE: chunk is defined and stream is a defined output stream
//POST: stream contains chunk
ostream & operator << (ostream & stream, const Block & chunk){
  stream << "[Start: " << chunk.getStart() << ", End: "
	 << chunk.getEnd() << ", NumWords: " << chunk.getSize() << "]";
  return stream;
}
