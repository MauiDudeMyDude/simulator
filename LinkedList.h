#ifndef INCLUDED_LinkedList
#define INCLUDED_LinkedList

#include "Node.h"
#include <iostream>
#include "constants.h"
using namespace std;

template <class T>
class LinkedList{
 private:
  Node<T> * head;
  Node<T> * tail;
  uint size;

  //PRE: index is less than size
  //POST: RV = a pointer to the node at index in the LinkedList
  Node<T> * getAtIndex(uint index) const{
    Node<T> * cursor;
    if (index >= size/2){
      cursor = tail;
      while(index < size - 1){
	cursor = cursor->getPrev();
	index++;
      }
    }else{
      cursor = head;
      while(index > 0){
	cursor = cursor->getNext();
	index--;
      }
    }
    return cursor;
  };
  
 public:
  //CONSTRUCOTRS
  //PRE: Default Constructor
  //POST: Initializes Variables
  LinkedList(){
    head = NULL;
    tail = NULL;
    size = 0;
  };
  
  //PRE: pData the data you want to store
  //POST: Initializes variables
  LinkedList(T pData){
    head = new Node<T>(pData);
    tail = head;
    size = 1;
  };

  //PRE: Copy Constructor
  //POST: Initializes Variables to be = to pList
  LinkedList(const LinkedList<T> & pList){
    head = NULL;
    tail = NULL;
    size = 0;
    Node<T> * cursor = pList.head;
    while (cursor != NULL){
      addToBack(cursor->getData());
      cursor = cursor->getNext();
    }
  };

  //DECONSTRUCTOR
  //PRE: Initialized
  //POST: No More
  ~LinkedList(){
    if (head != NULL){
      delete head;
    }
  };

  //ACCESSORS
  //PRE: Initialized
  //POST: RV = size
  int getSize()const{
    return size;
  };

  //PRE: Initialized
  //POST: RV = head
  Node<T> * getHead()const{
    return head;
  };

  //PRE: Initialized
  //POST: RV = tail
  Node<T> * getTail()const{
    return tail;
  };

  //ACTION METHODS
  //PRE: pData is what were looking for
  //POST: RV = the first index at which pData is and size if it is not in this
  uint find(const T pData){
    uint answer = 0;
    Node<T> * cursor = head;
    while((cursor != NULL) && !(cursor->getData() == pData)){
      answer++;
      cursor = cursor->getNext();
    }
    return answer;
  };

  
  //PRE: pData is what you want to add
  //POST: pData is put into a node and added to the end of the LinkedList
  void addToBack(const T pData){
    Node<T> * temp = new Node<T>(pData);
    if (size == 0){
      head = temp;
      tail = head;
    }else{
      tail->setNext(temp);
      temp->setPrev(tail);
      tail = temp;
    }
    size++;
  };

  //PRE: pData is what you want to add
  //POST: pData is put into a node and added to the begining of the LinkedList
  void addToFront(const T pData){
    Node<T> * temp = new Node<T>(pData);
    if(head != NULL){
      head->setPrev(temp);
      temp->setNext(head);
    }
    if(size == 0){
      tail = temp;
    }
    head = temp;
    head->setPrev(NULL);
    size++;
  };

  //PRE: pData is what you want to add and index is less than size
  //POST: pData is put into a node and added at index in the LinkedList
  void addAtIndex(const T pData, uint index){
    if(index == 0){
      addToFront(pData);
    }else if (index == size){
      addToBack(pData);
    }else{
      Node<T> * cursor = head;
      while(index >= 1){//1 instead of 0 because we already went through the
	               //first index when cursor = head was called
	cursor = cursor->getNext();
	index--;
      }
      Node<T> * temp = new Node<T>(pData);
      temp->setNext(cursor);
      temp->setPrev(cursor->getPrev());
      cursor->setPrev(temp);
      cursor = temp->getPrev();
      cursor->setNext(temp);
      size++;
    }
  };

  //PRE: index is the index you want
  //POST: RV = a reference to the value at index
  T & operator [] (uint index){
    Node<T> * cursor = getAtIndex(index);
    return cursor->getRefData();
  };

  //PRE: other's 1st element is the same as this's last element
  //POST: appends other to the end of this minus other's 1st element
  void addAtJoint(LinkedList<T> other){
    Node<T> * cursor = other.head->getNext();
    while(cursor != NULL){
      addToBack(cursor->getData());
      cursor = cursor->getNext();
    }
  };

  //PRE: index is the index you want
  //POST: returns the value stored at index
  T getData(uint index)const{
    Node<T> * cursor = getAtIndex(index);
    T answer = cursor->getData();
    return answer;
  };

  //PRE: Initialized
  //POST: RV = data of the node tail points to and the node is removed from the LinkedList
  T pop(){
    Node<T> * temp = tail;
    tail = temp->getPrev();
    T answer = temp->getData();
    temp->setNext(NULL);
    delete temp;
    size--;
    tail->setNext(NULL);
    return answer;
  };

  //PRE: Initialized
  //POST: RV = data of the node head points to and the node is removed from the LinkedList
  T remove(){
    Node<T> * temp = head;
    head = temp->getNext();
    if(head != NULL){
      head->setPrev(NULL);
    }else{
      tail = NULL;
    }
    T answer = temp->getData();
    temp->setNext(NULL);
    delete temp;
    size--;
    return answer;
  };

  //PRE: index is less than size
  //POST: RV = data of the node at index and remove it from the LinkedList
  T remove(const uint index){
    Node<T> * temp = getAtIndex(index);
    Node<T> * prev = temp->getPrev();
    Node<T> * next = temp->getNext();
    T answer;
    if (prev == NULL){
      answer = remove();
    }else if (next == NULL){
      answer = pop();
    }else{
      //ASSERT: prev and next cannot be NULL
      prev->setNext(next);
      next->setPrev(prev);
      temp->setNext(NULL);
      answer = temp->getData();
      delete temp;
      size--;
    }
    return answer;
  };

  //PRE: index1 is the index of the first node and index2 is the index of the second
  //POST: the Nodes at index1 and index2 swap places
  void swap(uint index1, uint index2){
    Node<T> * cursor = getAtIndex(index1);
    Node<T> * tempPrev = cursor->getPrev();
    Node<T> * tempNext = cursor->getNext();
    Node<T> * otherCursor = getAtIndex(index2);
    cursor->setNext(otherCursor->getNext());
    cursor->setPrev(otherCursor->getPrev());
    otherCursor->setNext(tempPrev);
    otherCursor->setPrev(tempNext);
  };

  //PRE: other is a reference to a list that you want to set this equal to
  //POST: RV = *this after you have set all of it's values equal to that of other
  LinkedList<T> & operator =(const LinkedList<T> & other){
    Node<T> * oldHead = head;
    Node<T> * cursor = other.head;
    head = NULL;
    tail = NULL;
    while(cursor != NULL){
      addToBack(cursor->getData());
      cursor = cursor->getNext();
    }
    size = other.getSize();
    if (oldHead != NULL){
      delete oldHead;
    }
    return *this;
  };

  //PRE: stream is a valid ostream and pList is initialized
  //POST: returns a ostream object with all of the data in the format [data1, data2, data3,..., dataN]
  friend ostream & operator <<(ostream & stream, const LinkedList<T> & pList){
    stream << "[";
    Node<T> * cursor = pList.head;
    while(cursor != NULL){
      stream << cursor->getData() << ", ";
      cursor = cursor->getNext();
    }
    stream << "]";
    return stream;
  }; 
  
};

#endif
