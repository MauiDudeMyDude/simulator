#ifndef INCLUDED_MyString
#define INCLUDED_MyString
#include <iostream>
#define uint unsigned int

using namespace std;

class MyString{
 private:
  char * characters;
  uint size;
  uint length;
  
 public:
  //PRE: Default constructor
  //POST: size is zero and characters contains an EOS character
  MyString();

  //PRE: constructor
  //POST: size is the size of the string and characters contains the string sent
  MyString(const char * word);

  //PRE: copy constructor
  //POST: all the values from pString have been properly copied over
  MyString(const MyString & pString);

  //PRE: deconstructor
  //POST: deletes everything
  ~MyString();

  //PRE: ch is a defined character
  //POST: characters is what it was with ch added just before the EOS character
  void addCharacter(char ch);
  
  //PRE: constructor has been called
  //POST: RV = size
  uint getSize() const;

  //PRE: constructor has been called
  //POST: RV = length
  uint getLength() const;

  //PRE: Initialized
  //POST: returns a MyString object containing this values but characters is in reverse order
  MyString  getReverse() const;
  
  //PRE: Initialized
  //POST: RV = a char[] == characters
  char * cstr() const;
  
  //PRE: pString is defined "\0" and stream is a defined input stream
  //POST: stream contains pString
  friend istream & operator >> (istream & stream, MyString & pString);

  //PRE: pString is defined "\0" and stream is a defined input stream
  //POST: stream contains pString
  friend ostream & operator << (ostream & stream, const MyString & pString);
  
  //PRE: pString is defined "...\0"
  //POST: RV = this concatenated with pString
  MyString operator + (const MyString & pString) const;
  
  //PRE: word is defined "..."
  //POST: RV = this concatenated with word
  MyString operator + (const char * word) const;
  
  //PRE: ch is defined 'x' x being some character
  //POST: RV = this concatenated with ch
  MyString operator + (const char ch) const;

  //PRE: index is an index of characters < size
  //POST: RV = characters[index]
  char & operator [] (const uint index);

  //PRE: pString is defined "...\0"
  //POST: RV = MyString that is not pString but defined "...\0"
  MyString & operator = (const MyString & pString);

  //PRE: word is defined "...\0"
  //POST: RV = MyString that is defined "...\0"
  MyString & operator = (const char * word);

  //PRE: pString is defined "...\0"
  //POST: RV = if this and pString contain the same String
  bool operator == (const MyString & pString) const;

  //PRE: word is defined "...\0"
  //POST: RV = if this and word contain the same String
  bool operator == (const char * word) const;

  //PRE: pString is defined "...\0"
  //POST: RV = this < pString, < meaning alphabeticaly
  bool operator < (const MyString & pString) const;
  
};

#endif
