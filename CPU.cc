#include "CPU.h"
#include "Memory.h"
#include "helper.h"
#include "constants.h"
#include "MyString.h"
#include "LinkedList.h"
#include "Block.h"
#include <iostream>
#include <fstream>
#include "Page.h"
#include "FileSystem.h"

using namespace std;

//==========Private=========//

//PRE: word2 might be negative and has 5 bits
//POST: RV = word1 + word2
uint CPU::addOffset(uint word1, uint word2){
  uint mask = 0x80000;
  bool w2 = false;
  if(mask & word2){
    word2 = word2 ^ 0xfffff;
    word2 += 0x1;
    w2 = true;
  }
  uint val;
  if(w2){
    val = word1 - word2;
  }else{
    val = word1 + word2;
  }
  return val;
}

//PRE: input contains "load 'prog'"
//POST: RV = a mystring containing 'prog'
MyString CPU::getProgName(MyString input){
  MyString prog;
  int cursor = 0;
  do{
    prog = prog + input[cursor + LOADOFFSET];
    cursor++;
  }while(input[cursor + LOADOFFSET] != EOS);
  return prog;
}

//PRE: pageQueue has at least one page
//POST: RV = the index in frameTable of pageQueue[0]
uint CPU::next(){
  uint answer;
  bool temp = true;
  uint index = 0;
  uint depth = 0;
  while((index < memory.getNumWords()/pagesize) && temp){
    if(frameTable[index] == pageQueue[depth]){
      if(!frameTable[index].getDirty()){
	temp = false;
	answer = index;
      }else{
	depth++;
	index = 0;
	continue;
      }
    }
    index++;
  }
  return answer;
}

//PRE: page may or may not be empty
//POST: RV = true iff page is empty and false otherwise
bool CPU::emptyPage(uint & page){
  bool answer = false;
  uint index = 0;
  while((index < memory.getNumWords()/pagesize) && (!answer)){
    if(frameTable[index].getEmpty()){
      answer = true;
      page = index;
    }
    index++;
  }
  return answer;
}

//PRE: pageTable is created
//POST: RV = index of the next place to put a page
uint CPU::findNextPage(){
  uint answer;
  if(!emptyPage(answer)){
    answer = next();
  }
  return answer;
}

//PRE: pg is a page to add to the frame Table page is the index
//     to place it at and temp is the PCB that holds pg
//POST: pg is in the frameTable
void CPU::addPage(Page pg, uint page, PCB temp){
  if(!frameTable[page].getEmpty()){
    pageQueue.remove();
  }
  pageQueue.addToBack(pg);
  frameTable[page] = pg;
  uint start = BYTESINWORD * pg.getID() * pagesize;
  fstream * file = temp.getProg();
  uint offset = file->tellg();
  file->seekp(start, ios::cur);
  for(int index = 0; index < pagesize; index++){
    uint word = 0x00000000;
    for(int bytesRead = 0; bytesRead < BYTESINWORD; bytesRead++){
      uint byte = (uint)(file->get());
      byte <<= 8 * (BYTESINWORD - bytesRead - 1);
      word += byte;
    }
    memory.addWord(word, index + (page * pagesize));
  }
  file->seekp(offset, ios::beg);
}


//PRE: prog is the name of the program. The first line of the file will
//     contain a positive integer, the number of words that the program in the
//     file uses, immediately followed by the new line character. The next
//     line onwards, the file will be a binary file, i.e., the words of the
//     LC-2200 machine language program will be written sequentially byte by
//     byte. There will not be any new line characters between words of the
//     machine language program. The end of file character will be the first
//     character on a new line.
//POST: Load the contents of the file prog.lce into memory starting at
//      address 0. When a program is loaded into memory, the PC should be set
//      to 0 and the stack pointer register should be set to the address of
//      the last word of memory.
MyString CPU::load(MyString & prog){
  MyString msg;
  PCB temp = PCB(pid, registers, prog, pagesize, stack);
  if(temp.getProg() == NULL){
    msg = (char*)("-Error: Program ");
    msg = msg + prog + (char*)(".lce does not exist");
  }else{
    runningQueue.addToBack(temp);
    pid++;
    uint page = findNextPage();
    Page pg = temp.getPageTable()[0];
    addPage(pg, page, temp);
    msg = (char*)"-program loaded";
  }
  return msg;
}

//PRE: start is where to start and similarly with end
//POST: Display the contents of memory, four words on each line, starting at
//      the word at address start and ending with the word at address end. If
//      start and/or end are not word addresses, i.e., multiples of 4, then
//      your program should give the user a suitable error message and then
//      show the simulator prompt. Your display should include the address of
//      the location and the contents, both in hexadecimal (8 digits) followed
//      by the decimal value of the contents in parentheses.
MyString CPU::mem(uint start, uint end){
  MyString msg;
  if((start % BYTESINWORD != 0) || (end % BYTESINWORD != 0)){
    msg = (char*)("-Error: start and end must be divisible by 4");
  }else{
    uint address = start;
    while(address <= end){
      int counter = 0;
      while((counter < BYTESINWORD) && (address <= end)){
	uint word = memory.getWord(address);
	char buffer[MAXMESSAGE];
	sprintf(buffer, "%d: %#08x (%d)  ", address, word, word);
	msg = msg + buffer;
	address += BYTESINWORD;
	counter++;
      }
      msg = msg + NL;
    }
  }
  return msg;
}

//PRE: only called if the shell is given "cpu"
//POST: Display the contents of the PC on one line. Following that, display
//      the contents of all the registers, four registers on each line. The
//      contents of the PC and the registers should be displayed in
//      hexadecimal (8 digits) and in decimal in parentheses. All the contents
//      should be suitably annotated, i.e., PC: 0x00000144, etc. When
//      displaying the registers, use the names for the registers, i.e., $a0,
//      $s0, etc. You may also find it useful to display in parentheses the
//      decimal values of the contents of the PC and registers.
MyString CPU::cpu(){
  char buffer[MAXMESSAGE];
  MyString msg;
  if(runningQueue.getSize() > 0){
    sprintf(buffer, "PID: %d ", runningQueue[0].getID());
  }else{
    sprintf(buffer, "PID: NONE ");
  }
  msg = buffer;
  sprintf(buffer, "PC: %#08x (%d) Stack: %d Timeslice: %d\n", pc, pc, stack, timeslice);
  msg = msg + buffer;
  uint index = 0;
  while (index < NUMREGISTERS){
    uint counter = 0;
    while((counter < BYTESINWORD) && (index < NUMREGISTERS)){
      sprintf(buffer, "%s: %#08x (%d) ", REGISTERNAMES[index], registers[index], registers[index]);
      msg = msg + buffer;
      counter++;
      index++;
    }
    msg = msg + NL;
  }
  return msg;
}

//PRE: step is how many steps of slice has been executed slice is the number
//     of time slices that have been run n is the number
//     of time slices to run line contains either a 0 or it contains a line
//     + a helper word to deterimine how to handle the program
//POST: run n time slices and return a MyString wich contains the outputs and
//      line will contain instructions to get input if needed
MyString CPU::runHelper(uint & step, uint & slice, uint n, MyString & line){
  MyString msg = ((char *)"");
  if(runningQueue.getSize() == 0){
    msg = (char*)("-Error: No Programs Loaded");
    line = DONE;
  }else{
    if((slice < n) && (runningQueue.getSize() > 0) && !(pullCommand(line) == INPUT)){
      for(uint index = 0; index < NUMREGISTERS; index++){
	registers[index] = runningQueue[0].getRegisters()[index];
      }
      msg = msg + runStep(step, line);
      if(!(pullCommand(line) == INPUT)){
	step = 0;
	slice ++;
      }
    }
    if(!(pullCommand(line) == INPUT)){
      if((slice == n) || (runningQueue.getSize() == 0)){
	line = DONE;
      }
    }
  }
  return msg;
}

//PRE: page may or may not be in the frameTable
//POST: RV = true iff page is in the frameTable and false otherwise
bool CPU::inFrameTable(Page page){
  bool answer = false;
  uint index = 0;
  while((index < pageQueue.getSize()) && !answer){
    if(frameTable[index] == page){
       answer = true;
    }
    index++;
  }
  return answer;
}

//PRE: page is not in frameTable or pageQueue
//POST: page is in the frameTable and pageQueue
void CPU::pageFault(Page page){
  uint next = findNextPage();
  addPage(page, next, runningQueue[0]);
}

//PRE: page is a page in frametable
//POST: RV = index of page
uint CPU::getMemWord(Page page){
  uint index = 0;
  while(!(page == frameTable[index])){
    index++;
  }
  return index;
}

//PRE: pc is the program counters current position
//     sw is true iff this was called in save word
//POST: RV = the location in memory pc is at and if
//      its not in memory it will load it in
uint CPU::getLocationInMemory(uint pc, bool sw){
  uint word = pc/BYTESINWORD;
  uint pageID = word/pagesize;
  Page page = runningQueue[0].getPageTable()[pageID];
  //ASSERT: page is the page that needs to be used
  if(!inFrameTable(page)){
    pageFault(page);
  }else{
    if(paging == 1){
      pageQueue.remove(pageQueue.find(page));
      pageQueue.addToBack(page);
    }
  }
  if(sw){
    pageQueue[pageQueue.find(page)].setDirty(true);
  }
  return ((word % pagesize) + (getMemWord(page) * pagesize)) * BYTESINWORD;
}

//PRE: step is how many steps of the current time slice has been run line
//     contains either a 0 or it contains a line + a helper word to determine
//     how to handle the program
//POST: Run 1 timeslices, or until the instruction halt is executed,
//      whichever comes earlier, of the machine language program, starting at
//      the current value of PC, and display the LC-2200 prompt.
MyString CPU::runStep(uint & step, MyString & line){
  MyString msg;
  bool halt = false;
  pc = runningQueue[0].getPC();
  //ASSERT: pc contains the actual pc based off the relative pc in the PCB
  while((step < timeslice) && (!halt)){
    uint location = getLocationInMemory(pc, false);
    uint instruction = memory.getWord(location);
    uint mask = 0xF0000000;
    uint opcode = instruction & mask;
    opcode >>= (BYTESINWORD * OPCODESHIFT);
    mask = 0x0F000000;
    uint regx = instruction & mask;
    regx >>= (BYTESINWORD * REGXSHIFT);
    mask = 0x00F00000;
    uint regy = instruction & mask;
    regy >>= (BYTESINWORD * REGYSHIFT);
    mask = 0x000FFFFF;
    uint number = instruction & mask;
    //ASSERT: opcode contains the opcode, regx contains the first register,
    //        regy contains the second register, number contains the number or offset given
    pc += BYTESINWORD;
    
    //Testing couts
    /*
      cout << "=======================================" << endl;
      cout << "PC: " << pc << endl;
      cout << "instruction: " << hex << instruction << endl;
      cout << "opcode: " << hex << opcode << endl;
      cout << "regx: " << hex << regx << endl;
      cout << "regy: " << hex << regy << endl;
      cout << "number/offset: " << hex << number << endl;
      cout << "=======================================" << endl;
    */
    
    //Handle Opcodes
    switch((opcodes)opcode){
    case OUT:
      //OUT CODE
      char buffer[MAXMESSAGE];
      sprintf(buffer, "%d) %d\n", runningQueue[0].getID(), registers[regx]);
      msg = msg + buffer;
      break;
    case ADD:
      //ADD CODE
      registers[regx] = registers[regy] + registers[number];
      break;
    case NAND:
      //NAND CODE
      registers[regx] = ~(registers[regy] & registers[number]);
      break;
    case ADDI:
      //ADDI CODE
      registers[regx] = addOffset(registers[regy], number);
      break;
    case LW:
      uint lwWord;
      //LW CODE
      if(registers[regy] + number < runningQueue[0].getSize() * BYTESINWORD){
	lwWord = getLocationInMemory(registers[regy] + number, false);
	registers[regx] = memory.getWord(lwWord);
      }else{
	msg = "-Error: Out of bounds on LW";
	halt = true;
      }
      break;
    case SW:
      uint swWord;
      //SW CODE
      if(registers[regy] + number < runningQueue[0].getSize() * BYTESINWORD){
        swWord = getLocationInMemory(registers[regy] + number, true);
	memory.setWord(registers[regx], swWord);
      }else{
	msg = "-Error: Out of bounds on SW";
	halt = true;
      }
      break;
    case BEQ:
      //BEQ CODE
      if(registers[regx] == registers[regy]){
	pc = addOffset(pc, number);
      }
      break;
    case JALR:
      //JARL CODE
      registers[regy] = pc;
      pc = registers[regx];
      break;
    case HALT:
      //HALT CODE
      halt = true;
      break;
    case LA: {
      //LA CODE
      registers[regx] = number;
      break;}
    case IN: {
      //IN CODE
      if(numArguements(line) != 2){
	//ASSERT: program needs input
	line = INPUT + (char*) " "  + convertToString(pc - BYTESINWORD) + (char*)" " + convertToString(runningQueue[0].getID());
	pc -= BYTESINWORD;
	step--;
	halt = true;
      }else{
	//ASSERT: program got input
	registers[regx] = getAttribute(line, 1);
	line = convertToString(pc);
      }
      break; }
    case BGT:
      //BGT CODE
      if(registers[regx] > registers[regy]){
	pc = addOffset(pc, number);
      }
      break;
    }
    step++;
  }
  //ASSERT: timeslice has finished or the program has halted
  runningQueue[0].setPC(pc);
  uint * regs = runningQueue[0].getRegisters();
  for(uint index = 0; index < NUMREGISTERS; index++){
    regs[index] = registers[index];
  }
  //ASSERT: local pc and registers saved to PCB
  if(numArguements(line) == 1){
    if(halt){
      char add[MAXMESSAGE];
      sprintf(add, "-program %d halted", runningQueue[0].getID());
      msg = msg + add;
      kill(runningQueue[0].getID());
      line = HALTED;
      //ASSERT: program has halted so it was removed from the running queue and dealocated
    }else if(runningQueue.getSize() > 1){
      PCB temp = runningQueue.remove(0);
      runningQueue.addToBack(temp);
      //ASSERT: program has moved from the front of the queue to the back
    }
  }
  
  return msg;
}

//PRE: line contains either a 0 or a number + a helper command to help run the program
//POST: Run the machine language program, starting at the current value of PC,
//      until, if ever, the instruction halt is executed.
/*MyString CPU::run(MyString & line){
  return runStep(memory.getNumWords(), line);
  }*/

//PRE: Initialized
//POST: Display the process information for all the processes in the running
//      queue, starting with the process at the front of the queue. For each
//      process, the simulator should print out the PID for the process, the
//      name of the program for the process, the PC for the process, the starting
//      address of the memory allocaated to the process, the number of words of
//      memory allocated to the process, the starting address of stack space
//      allocated to the process, the ending address of the stack space allocated
//      to the process, and the value of the stack pointer for the process. 
MyString CPU::jobs(){
  MyString msg;
  char buffer[MAXMESSAGE];
  uint numJobs = runningQueue.getSize();
  for(uint index = 0; index < numJobs; index++){
    PCB job = runningQueue[index];
    sprintf(buffer, "\nPID: %d Name: ", job.getID());
    msg = msg + buffer + job.getName();
    sprintf(buffer, " PC: %d \n", job.getPC());
    msg = msg + buffer;
    sprintf(buffer, "$sp: %#08x (%d)\n", job.getRegisters()[SP], job.getRegisters()[SP]);
    msg = msg + buffer;
    uint ind = 0;
    while (ind < NUMREGISTERS){
      uint counter = 0;
      while((counter < BYTESINWORD) && (ind < NUMREGISTERS)){
	sprintf(buffer, "%s: %#08x (%d) ", REGISTERNAMES[ind], runningQueue[index].getRegisters()[ind], runningQueue[index].getRegisters()[ind]);
	msg = msg + buffer;
	counter++;
	ind++;
      }
      msg = msg + NL;
    }
  }
  if(numJobs == 0){
    msg = (char*)("-Error: There aren't any programs loaded");
  }
  return msg;
}

//PRE: pid may be the pid of a job
//POST: RV = index of job with pid and size if job doesn't exist
uint CPU::getJob(uint pid){
  uint index = 0;
  bool found = false;
  PCB job;
  while((!found) && (index < runningQueue.getSize())){
    job = runningQueue[index];
    if(job.getID() == pid){
      found = true;
    }else{
      index++;
    }
  }
  return index;
}

//PRE: pid is the job id to remove from ram
//POST: the job with pid of pid has been removed from ram
void CPU::ramDealocate(uint pid){
  for(int index = 0; index < memory.getNumWords()/pagesize; index++){
    if(frameTable[index].getPID() == pid){
      frameTable[index].setEmpty(true);
    }
    if(pageQueue[index].getPID() == pid){
      pageQueue.remove(index);
    }
  }
}

//PRE: Initialized
//POST: Remove from the running queue the process, if any, with the specified PID,
//      and free all memory, including stack space, allocated to that process. 
MyString CPU::kill(uint id){
  MyString msg;
  uint numJobs = runningQueue.getSize();
  uint index = getJob(id);
  if(index < numJobs){
    ramDealocate(id);
    runningQueue.remove(index);
    msg = (char*)("-killed");
  }else{
    msg = (char*)("-Error: That PID doesn't exist");
  }
  return msg;
}

//PRE: frame table is initialized
//POST: RV = a Mystring containing the frame table
MyString CPU::printFrameTable(){
  MyString msg;
  char buffer[MAXMESSAGE];
  for(int index = 0; index < memory.getNumWords()/pagesize; index++){
    msg = msg + (char*)"=====================\n";
    sprintf(buffer, "PID %d ID: %d Stack: ", frameTable[index].getPID(), frameTable[index].getID());
    msg = msg + buffer;
    if(frameTable[index].getStackSpace()){
      msg = msg + (char*)"true ";
    }else{
      msg = msg + (char*)"false ";
    }
    msg = msg + (char*)"Dirty: ";
    if(frameTable[index].getDirty()){
      msg = msg + (char*)"true ";
    }else{
      msg = msg + (char*)"false ";
    }
    msg = msg + (char*)"Empty: ";
    if(frameTable[index].getEmpty()){
      msg = msg + (char*)"true";
    }else{
      msg = msg + (char*)"false";
    }
    msg = msg + (char*)"\n";
  }
  return msg;
}

//PRE: pid may be the pid of a job
//POST: RV = a mystring containing the page table of job pid
MyString CPU::printPageTable(uint pid){
  MyString msg;
  int index = getJob(pid);
  if(index < runningQueue.getSize()){
    msg = msg + runningQueue[index].printPageTable();
  }else{
    msg = (char*)("-Error: That PID doesn't exist");
  }
  return msg;
}

//===========Public==========//

//PRE: all parameters contain the values that were preset in the .lc_config file
//POST: This object has been initialized
CPU::CPU(uint wordsOfMemory, uint wordsOfStack, uint pTimeslice, uint pPageSize,
	 uint pSwapSpace, uint pPaging, uint fileSysSize){
  memory.setNumWords(wordsOfMemory);
  stack = wordsOfStack;
  timeslice = pTimeslice;
  pagesize = pPageSize;
  swapspace = pSwapSpace;
  paging = pPaging;
  pc = 0;
  pid = 0;
  freemem.addToBack(Block(0, (wordsOfMemory - 1) * BYTESINWORD, wordsOfMemory));
  registers = new uint[NUMREGISTERS];
  for(int index = 0; index < NUMREGISTERS; index++){
    registers[index] = 0;
  }
  frameTable = new Page[wordsOfMemory/pagesize];
  swap_space = new fstream(".lc_swapspace", ios::in | ios::out);
  FileSystem temp(fileSysSize);
  files = temp;
}

//Deconstructor
CPU::~CPU(){
  delete[] registers;
  delete[] frameTable;
  delete swap_space;
}

//PRE: step and slice contain info only for step input contains a proper command
//     and any atributes that go with it msg contains garbage and line contains the line to start at
//POST: command was processed
void CPU::execute(uint & step, uint & slice, MyString input, MyString & msg, MyString & line){
  MyString command;
  command = pullCommand(input);
  uint numA = numArguements(input);
  if(command == COMMANDS[LOADINDEX]){
    //Load
    if(numA != 2){
      msg = (char*)("-Error: Wrong number of attributes");
    }else{
      MyString prog = getProgName(input);
      msg = load(prog);
    }
    line = DONE;
  }else if(command == COMMANDS[MEMINDEX]){
    //Mem
    if(numA == 3){
      uint start = getAttribute(input, 1);
      uint end = getAttribute(input, 2);
      msg = mem(start, end);
    }else if(numA == 2){
      uint start = getAttribute(input, 1);
      msg = mem(start, (memory.getNumWords() - 1) * BYTESINWORD);
    }else if(numA == 1){
      msg = mem(0, (memory.getNumWords() - 1) * BYTESINWORD);
    }else{
      msg = (char*)("-Error: Wrong number of attributes");
    }
    line = DONE;
  }else if(command == COMMANDS[CPUINDEX]){
    //Cpu
    if(numA != 1){
      msg = (char*)("-Error: Wrong number of attributes");
    }else{
      msg = cpu();
    }
    line = DONE;
  }else if(command == COMMANDS[STEPINDEX]){
    //Step
    if(numA != 2){
      msg = (char*)("-Error: Wrong number of attributes");
      line = DONE;
    }else{
      uint attr = getAttribute(input, 1);
      msg = runHelper(step, slice, attr, line);
    }
  }else if(command == COMMANDS[RUNINDEX]){
    //Run
    /*if(numA != 1){
      msg = (char*)("Wrong number of attributes");
    }else{
      msg = run(line);
      }*/
    msg = (char*)("-Error: 'run' is no longer a command");
    line = DONE;
  }else if(command == COMMANDS[FREEMEMINDEX]){
    //Freemem
    msg = (char*)("-Error: 'freemem' is no longer a command");
    line = DONE;
  }else if(command == COMMANDS[JOBSINDEX]){
    //Jobs
    if(numA != 1){
      msg = (char*)("-Error: Wrong number of attributes");
    }else{
      msg = jobs();
    }
    line = DONE;
  }else if(command == COMMANDS[KILLINDEX]){
    //Kill
    if(numA != 2){
      msg = (char*)("-Error: Wrong number of attributes");
    }else{
      uint id = getAttribute(input, 1);
      msg = kill(id);
    }
    line = DONE;
  }else if(command == COMMANDS[PAGESINDEX]){
    //Pages
    if(numA == 1){
      msg = printFrameTable();
    }else if(numA == 2){
      msg = printPageTable(getAttribute(input, 1));
    }else{
      msg = (char*)("-Error: Wrong number of attributes");
    }
    line = DONE;
  }else if(command == COMMANDS[FORMATINDEX]){
    //Format
    if(numA > 1){
      msg = (char*)("-Error: Wrong number of attributes");
    }else{
      files.format();
      msg = (char*)("-Files Formatted");
    }
    line = DONE;
  }else if(command == COMMANDS[PWDINDEX]){
    //Pwd
    if(numA > 1){
      msg = (char*)("-Error: Wrong number of attributes");
    }else{
      msg = files.pwd();
    }
    line = DONE;
  }else if(command == COMMANDS[LSINDEX]){
    //Ls
    if(numA <= 2){
      msg = (char*)("-This command will display the contents of the current directory");
    }else{
      msg = (char*)("-Error: Wrong number of attributes");
    }
    line = DONE;
  }else if(command == COMMANDS[CDINDEX]){
    //Cd
    if(numA <= 2){
      msg = (char*)("-This command will change to the directory specified in the arguement");
    }else{
      msg = (char*)("-Error: Wrong number of attributes");
    }
    line = DONE;
  }else if(command == COMMANDS[MKDIRINDEX]){
    //Mkdir
    if(numA == 2){
      msg = (char*)("-This command will create a directory at the locataion specified");
    }else{
      msg = (char*)("-Error: Wrong number of attributes");
    }
    line = DONE;
  }else if(command == COMMANDS[RMDIRINDEX]){
    //Rmdir
    if(numA == 2){
      msg = (char*)("-This command will delete the directory at the path described");
    }else{
      msg = (char*)("-Error: Wrong number of attributes");
    }
    line = DONE;
  }else if(command == COMMANDS[DISPLAYINODEINDEX]){
    //DisplayInode
    if(numA <= 3){
      uint start = 0;
      uint end = files.getNumInodes();
      if(numA = 3){
	start = getAttribute(input, 1);
	end = getAttribute(input, 2);
      }else if(numA = 2){
	start = getAttribute(input, 1);
      }
      msg = files.displayInode(start, end);
    }else{
      msg = (char*)("-Error: Wrong number of attributes");
    }
    line = DONE;
  }
}
