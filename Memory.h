#ifndef INCLUDED_Memory
#define INCLUDED_Memory
#include "constants.h"
#include "LinkedList.h"
#include "Block.h"

class Memory{

 private:
  //variables
  uint * memory;
  uint numWords;

 public:
   //Default Constructor
   Memory();

  //Constructor
  Memory(uint pNumWords);

  //Destructor
  ~Memory();
  
  //PRE: word is a full word of memory and index is where to insert it into mem
  //POST: word is stored at memory[index]
  void addWord(uint word, uint index);

  //PRE: initialized
  //POST: RV = numWords
  uint getNumWords();

  //PRE: pNumWords is how many words of memory you want this to hold
  //POST: numWords = pNumWords and memory is a empty uint[numWords]
  void setNumWords(uint pNumWords);

  //PRE: address is a valid address of a word in memory
  //POST: RV = the word at address address
  uint getWord(uint address);

  //PRE: word is no more then 8 bytes and address is a valid address of a word
  //     in memory
  //POST: stores word at the address address
  void setWord(uint word, uint address);
};

#endif
