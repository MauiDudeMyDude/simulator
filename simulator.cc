#include <iostream>
#include "helper.h"
#include "CPU.h"
#include "constants.h"

using namespace std;

//This program is a simulation of a LC2200
//features:
//    -run Assembly Language programs
//    -run multiple programs with time slicing
//    -uses pageing for memory management
int main(int argc, char * argv[]){

  if(argc != 1){
    //ASSERT: The program was called incorrectly
    cout << "Usage: ./LC2200" << endl;
  }else{
    //ASSERT: The program was called correctly
    uint config[CONFIGARGUEMENTS];
    readConfig(config);
    //ASSERT: config contains all arguements from the .lc_config file
    cout << "-config loaded" << endl;
    CPU computer(config[0], config[1], config[2], config[3], config[4], config[5], config[6]);
    //ASSERT: computer is a CPU object created with the config arguements
    cout << "-computer created" << endl << "SHELL:" << endl;
    shell(computer);
    cout << "Shutting Down, Have a nice day :)" << endl;
    //ASSERT: "exit" was typed in the shell
  }

  return 0;
}
