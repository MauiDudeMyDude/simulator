#ifndef INCLUDED_Block
#define INCLUDED_Block
#include "constants.h"
#include <iostream>

using namespace std;

class Block{
 private:
  uint start;
  uint end;
  uint size;

 public:
  //Default Constructor
  Block();

  //Constructor
  Block(uint pStart, uint pEnd, uint pSize);

  //getters
  //PRE: start defined
  //POST: RV = start
  uint getStart()const;

  //PRE: end defined
  //POST: RV = end
  uint getEnd()const;

  //PRE: size defined
  //POST: RV = size
  uint getSize()const;

  //setters
  //PRE: defined
  //POST: start = pStart
  void setStart(const uint pStart);

  //PRE: defined
  //POST: end = pEnd
  void setEnd(const uint pEnd);

  //PRE: defined
  //POST: size = pSize
  void setSize(const uint pSize);

  //methods
  //PRE: chunk is no larger than size
  //POST: size = size - chunk and start = the new starting address
  void reduce(const uint chunk);

  //PRE: chunk is defined and stream is a defined output stream
  //POST: stream contains chunk
  friend ostream & operator << (ostream & stream, const Block & chunk);

};

#endif
