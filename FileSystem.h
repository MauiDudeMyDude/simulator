#ifndef INCLUDED_FILESYSTEM
#define INCLUDED_FILESYSTEM

#include "MyString.h"
#include "Inode.h"
#include "constants.h"

class FileSystem{
 private:
  MyString PWD;
  Inode * inodes;
  uint numInodes;
  uint currentInode;

  //PRE: path is the path to the directory you want
  //POST: RV = the index of the inode at the directory defined by path
  uint getIndexAt(MyString path);
  
 public:
  //Default Constructor
  FileSystem();
  
  //PRE: pNumInodes is greater than 2
  //POST: pwd = "/" inodes = new Inode[pNumInodes]
  //      numInodes = pNumInodes inodes[0] will be
  //      a super inode and inodes[1] will be the root
  //      directory inode
  FileSystem(uint pNumInodes);

  //PRE: defined
  //POST: RV = numInodes
  uint getNumInodes();

  //PRE: inodes is defined
  //POST: inodes[0] will be the super inode, inodes[1] will
  //      be the root directory with no children, and all
  //      other inodes will be free and in order
  //      2, 3, ..., numInodes - 1
  void format();

  //PRE: defined
  //POST: RV = pwd
  MyString pwd();

  //PRE: path is a path to the directory wanted
  //POST: lists the contents of the directory defined by path
  MyString ls(MyString path);

  //PRE: path is a path to the directory wanted
  //POST: changes to the directory specified by path. if path
  //      is an invalid directory an error message will be returned
  //      otherwise a success message will be returned
  MyString cd(MyString path);

  //PRE: path is a path to the directory wanted
  //POST: if path is a valid path to create the directory it will be
  //      created with the first free inode. if there are no more free
  //      inodes or the path is invalid a proper error message will be
  //      returned otherwise a success message will be returned
  MyString mkdir(MyString path);

  //PRE: path is a path to the directory wanted
  //POST: if the directory exists and has no children it will be deleted
  //      and a success message will be returned otherwise a proper error
  //      error message will be returned
  MyString rmdir(MyString path);

  //PRE: start is less than or equal to end
  //POST: RV = all inodes from start to end will be formated like
  //      "Inode No. 'Inode Index'
  //       'word 1' 'word 2' 'word 3' 'word 4'
  //       'word 5' 'word 6' 'word 7' 'word 8'"
  //      in to a MyString
  MyString displayInode(uint start, uint end);
  
};

#endif
