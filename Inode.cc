#include "Inode.h"
#include "MyString.h"
#include "constants.h"

//PRE: Constructor
//POST: Makes this an empty Inode
Inode::Inode(){
  for(uint word = 0; word < INODEWORDS; word++){
    for(uint byte = 0; byte < BYTESINWORD; byte++){
      data[word][byte] = 0;
    }
  }
}

//PRE: word is the word the byte is in and byte is
//     which byte you want
//POST: RV = the contents of byte byte in word word
//      of data
uint Inode::getByte(uint word, uint byte){
  return data[word][byte];
}

//PRE: word is the word the bytes are in and firstByte is
//     the first byte you want of data
//POST: RV = the contents of bytes firstByte and firstByte + 1
//      as if they were 1 in word word of data
uint Inode::getBytes(uint word, uint firstByte){
  return (data[word][firstByte] * TWOBYTES) + data[word][firstByte + 1];
}

//PRE: bytetwo is zero and num is the number that
//     should be split into two bytes
//POST: byte two contains the left byte and num
//      contains the right byte
void Inode::split(uint & bytetwo, uint & num){
  while(num >= TWOBYTES){
    num -= (TWOBYTES - 1);
    bytetwo++;
  }
}

//PRE: word is the word and byte is the byte of
//     data you want to hold val 
//POST: byte byte of word word of data = val
void Inode::setByte(uint word, uint byte, uint val){
  data[word][byte] = val;
}

//PRE: word is the word and firstByte is the first byte of
//     data you want to hold val
//POST: bytes firstByte and firstByte+1 of word word are set
//      to val
void Inode::setBytes(uint word, uint firstByte, uint val){
  uint bytetwo = 0;
  split(bytetwo, val);
  data[word][firstByte] = bytetwo;
  data[word][firstByte + 1] = val;
}
